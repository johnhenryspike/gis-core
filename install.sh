#!/bin/bash

# variables
USER="sgis"
DBNAME="db_sgis"
DB_PASS="qzwxec"
LOGS_FILE=$(mktemp /tmp/gis_core_installation-XXXXXX.log)

gis_core_vars(){
  # messages
  MGC001="Log file path: "
  MGC002="Check ubuntu-gis ppa repository"
  MGC003="Ubuntu-gis ppa repository is already configured on the server."
  MGC004="Getting ubuntu-gis ppa repository. Please wait..."
  MGC005="Software installation. Please wait..."
  MGC006="Configuration NginX"
  MGC007="Configuration PHP-FPM"
  MGC008="Configuration MapServer"
  MGC009="Configuration MapCache"
  MGC009="Restarting services. Please wait..."
  MGC010="Software installation complete."
  MGC011="Configuration Memcached."
  MGC012="Configuration Postgresql."
  MGC013="Installation failed! Please view log file for details."
  MGC014="Download installation dependenses. Please wait..."
}

print(){
    # print helper
    msg=$1
    notice=${2:-0}
    [[ $notice -eq 1 ]] && echo -e "${msg}"
    [[ $notice -eq 2 ]] && echo -e "\e[1;31m${msg}\e[0m"
    [[ $notice -eq 3 ]] && echo -e "\e[1;32m${msg}\e[0m"
    echo "$(date +"%FT%H:%M:%S"): $$ : $msg" >> "$LOGS_FILE"
}

fail(){
  # fail install helper
  print "$MGC013" 2
  exit 1
}

download_deps(){
  print "$MGC014" 1
  mkdir gci
  mkdir gci/configs
  wget -o "$LOGS_FILE" https://gitlab.com/johnhenryspike/gis-core/-/raw/master/configs/memcached.conf -O gci/configs/memcached.conf
  wget -o "$LOGS_FILE" https://gitlab.com/johnhenryspike/gis-core/-/raw/master/configs/sgis_main.conf -O gci/configs/sgis_main.conf
  wget -o "$LOGS_FILE" https://gitlab.com/johnhenryspike/gis-core/-/raw/master/configs/sgis_php.conf -O gci/configs/sgis_php.conf
  wget -o "$LOGS_FILE" https://gitlab.com/johnhenryspike/gis-core/-/raw/master/configs/sgis.conf -O gci/configs/sgis.conf
  wget -o "$LOGS_FILE" https://gitlab.com/johnhenryspike/gis-core/-/raw/master/configs/pg_hba.conf -O gci/configs/pg_hba.conf
  wget -o "$LOGS_FILE" https://gitlab.com/johnhenryspike/gis-core/-/raw/master/configs/mapserv.fcgiwrap -O gci/configs/mapserv.fcgiwrap
  wget -o "$LOGS_FILE" https://gitlab.com/johnhenryspike/gis-core/-/raw/master/configs/mapcache.fcgiwrap -O gci/configs/mapcache.fcgiwrap
}

check_repo(){
  print "$MGC002" 1

  #check that file with ubuntugis ppa repo exist
	PPAGIS=$(grep -Ehr '^deb.*ubuntugis' /etc/apt/* | grep -c '^deb.*ubuntugis')
  if [[ "$PPAGIS" -gt 0 ]]; then
      print "$MGC003" 3
      return 0
  fi
  print "$MGC004" 2

  #add ubuntugis repo if not exist
  add-apt-repository -y ppa:ubuntugis/ppa >> "$LOGS_FILE"
}

install_all(){
  print "$MGC005" 3

  # update packages
  apt-get -yqq update
  # >> "$LOGS_FILE"
  apt-get -yqq upgrade
  #>> "$LOGS_FILE"

  #install required packages
  apt-get install -y nginx postgis postgresql \
  php7.2-{curl,fpm,gd,json,intl,pgsql,xml} \
  mapserver-bin mapcache-cgi mapcache-tools fcgiwrap memcached
  #>> "$LOGS_FILE"
  apt autoremove
  print "check software installed" 1
  php -v
  psql -V
  nginx -v
  mapserv -v
  # nvm installation
  wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash >> "$LOGS_FILE"
  print "$MGC010" 3
}

add_user(){
  useradd -m $USER
}

create_dirs(){
  # create systems dir's
  mkdir /home/$USER/log
  mkdir /home/$USER/log/nginx
  mkdir /home/$USER/log/memcached
  mkdir /home/$USER/frontend
  mkdir /home/$USER/api
  mkdir /home/$USER/api/public
  mkdir /home/$USER/admin

  # create blobs for default path's
  echo "put_frontend_here" > /home/$USER/frontend/index.html
  echo "put_admin_here" > /home/$USER/admin/index.html
  echo "<?php echo 'put_php_here'; phpinfo()?> " > /home/$USER/api/public/index.php

  # set permissions for created files and dir's
  chown -R $USER:$USER /home/$USER
}

configure_nginx(){
  print "$MGC006" 1
  # create servers configs
  sed -e "s;%USER%;$USER;g" gci/configs/sgis_php.conf  > /etc/nginx/sites-available/sgis_php.conf
  sed -e "s;%USER%;$USER;g" gci/configs/sgis_main.conf  > /etc/nginx/sites-available/sgis_main.conf

  # symlinks for server configs
  ln -s /etc/nginx/sites-available/sgis_main.conf /etc/nginx/sites-enabled/sgis_main.conf
  ln -s /etc/nginx/sites-available/sgis_php.conf /etc/nginx/sites-enabled/sgis_php.conf
}

configure_php(){
  print "$MGC007" 1
  sed -e "s;%USER%;$USER;g" gci/configs/sgis.conf > /etc/php/7.2/fpm/pool.d/sgis.conf
}

configure_db(){
  print "$MGC012" 1
  service postgresql restart
  # create role and user with password for sgis system
  if ! echo "CREATE ROLE $USER LOGIN ENCRYPTED PASSWORD '$DB_PASS';" | sudo -u postgres psql -w -Upostgres
  then
    fail
  fi
  print "role created" 1
  # postgresql need to have db named username/role
  # create this
  if ! sudo -u postgres createdb -Upostgres $USER --owner $USER
  then
    fail
  fi
  print "db created" 1
  # create database for sgis system
  if ! sudo -u postgres createdb -Upostgres $DBNAME --owner $USER
  then
    fail
  fi

  # get path to pg_hba.conf
  HBA_PATH=$(echo "SHOW hba_file;" | sudo -u postgres psql -Upostgres | grep /etc | sed -e "s/^[[:space:]]*//")

  # create bak file for default pg_hba.conf
  if ! cp "$HBA_PATH" "$HBA_PATH".bak
  then
    fail
  fi

  # create new pg_hba.conf for sgis system
  sed -e "s;%USER%;$USER;g" gci/configs/pg_hba.conf > "$HBA_PATH"
}

configure_memcached(){
  print "$MGC011" 1
  sed -e "s;%USER%;$USER;g" gci/configs/memcached.conf > /etc/memcached_memmapcache.conf

  # remove for enable multiply configs
  rm /lib/systemd/system/memcached.service
}

configure_mapserv(){
  print "$MGC008" 1
  sed -e "s;%USER%;$USER;g" gci/configs/mapserv.fcgiwrap > /etc/init.d/mapserv
  chmod +x /etc/init.d/mapserv
}

configure_mapcache(){
  print "$MGC009" 1
  sed -e "s;%USER%;$USER;g" gci/configs/mapcache.fcgiwrap > /etc/init.d/mapcache
  chmod +x /etc/init.d/mapcache
}

restart_services(){
  print "$MGC010" 1
  systemctl daemon-reload
  service php7.2-fpm restart
  service nginx restart
  service mapserv start
  service mapcache start
  service memcached restart
  service postgresql restart
}

welcome(){
  print "installation successfull." 3
  print "Service available at http://localhost" 1
  print "Documentation available at http://localhost/api/documentation" 1
  print "login: admin" 1
  print "pass: admin" 1
}

gis_core_vars

print "====================================================================" 3
print "===============       GIS-CORE v0.1 INSTALLER        ===============" 3
print "====================================================================" 3
print "$MGC001"  1
print "$LOGS_FILE" 1
print "GIS-CORE installation started" 1
echo 'debconf debconf/frontend select Noninteractive' | debconf-set-selections


download_deps
check_repo

install_all
add_user
create_dirs

configure_nginx
configure_db
configure_php
configure_mapserv
configure_mapcache
configure_memcached

restart_services

welcome