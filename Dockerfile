FROM ubuntu:bionic
ENV DEBIAN_FRONTEND noninteractive
RUN apt-get update && apt-get install -y apt-utils wget sudo software-properties-common openssh-server

COPY configs/sgis_php.conf /tmp/
COPY configs/sgis.conf /tmp/
COPY configs/nginx.conf /tmp/
COPY configs/sgis_main.conf /tmp/
COPY configs/pg_hba.conf /tmp/
COPY configs/run.sh /root/

RUN add-apt-repository -y ppa:ubuntugis/ppa
RUN apt-get -yqq update
RUN apt-get -yqq upgrade

RUN apt-get install -y \
    php7.2-curl php7.2-fpm php7.2-gd php7.2-json php7.2-intl php7.2-pgsql php7.2-xml \
    nginx postgis postgresql \
    mapserver-bin mapcache-cgi mapcache-tools fcgiwrap memcached

RUN apt -y autoremove

RUN wget -qO- https://raw.githubusercontent.com/nvm-sh/nvm/v0.35.2/install.sh | bash
RUN useradd -m sgis

VOLUME /home/sgis/frontend

RUN mkdir /home/sgis/log
RUN mkdir /home/sgis/admin
#RUN mkdir /home/sgis/frontend
RUN mkdir /home/sgis/api
RUN mkdir /home/sgis/log/nginx
RUN mkdir /home/sgis/log/memcached
RUN mkdir /home/sgis/api/public

RUN wget

#RUN echo "put_frontend_here" > /home/sgis/frontend/index2.html
RUN echo "put_admin_here" > /home/sgis/admin/index.html
RUN echo "<?php echo 'put_php_here'; phpinfo()?> " > /home/sgis/api/public/index.php
RUN chown -R sgis:sgis /home/sgis

RUN sed -e "s;%USER%;sgis;g" /tmp/nginx.conf  > /etc/nginx/nginx.conf
RUN sed -e "s;%USER%;sgis;g" /tmp/sgis_php.conf  > /etc/nginx/sites-available/sgis_php.conf
RUN sed -e "s;%USER%;sgis;g" /tmp/sgis_main.conf  > /etc/nginx/sites-available/sgis_main.conf
RUN ln -s /etc/nginx/sites-available/sgis_main.conf /etc/nginx/sites-enabled/sgis_main.conf
RUN ln -s /etc/nginx/sites-available/sgis_php.conf /etc/nginx/sites-enabled/sgis_php.conf

RUN sed -e "s;%USER%;sgis;g" /tmp/sgis.conf > /etc/php/7.2/fpm/pool.d/sgis.conf
CMD sudo -u postgres psql -w -Upostgres -c "CREATE ROLE sgis LOGIN ENCRYPTED PASSWORD 'qzwxec';"
CMD sudo -u postgres createdb -Upostgres sgis --owner=sgis
CMD sudo -u postgres createdb -Upostgres sgisdb --owner=sgis
RUN cp "/etc/postgresql/10/main/pg_hba.conf" "/etc/postgresql/10/main/pg_hba.conf.bak"
RUN sed -e "s;%USER%;sgis;g" /tmp/pg_hba.conf > "/etc/postgresql/10/main/pg_hba.conf"
EXPOSE 88
RUN echo "nginx_access_log_file" > "/home/sgis/log/nginx/sgis_main.access.log"
RUN chmod +x /root/run.sh
CMD ./root/run.sh