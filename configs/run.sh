#!/bin/bash
service postgresql start &
service php7.2-fpm start &
service nginx start &
tail -f /root/run.sh